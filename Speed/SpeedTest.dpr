program SpeedTest;

{$APPTYPE CONSOLE}

{$R *.res}

{$DEFINE SPRING4D}
{$DEFINE JCL}

uses
  System.Classes,
  System.Diagnostics,
  System.SysUtils,
  {$IFDEF SPRING4D}
  Spring,
  {$ENDIF}
  {$IFDEF JCL}
  JclSysUtils,
  {$ENDIF}
  SmartPtr;


const
  ADD_COUNT = 200;
  CREATE_COUNT = 100000;

{$IFDEF SPRING4D}
procedure MeasureSpring;
begin
  var s := Shared.Make(TStringList.Create);
  for var i := 1 to ADD_COUNT do
    s.Add(i.ToString);
end;

procedure MeasureSpringCopy;
begin
  var s := Shared.Make(TStringList.Create);
  for var i := 1 to ADD_COUNT do begin
    var s1 := s;
    s1.Add(i.ToString);
  end;
end;
{$ENDIF}

{$IFDEF JCL}
procedure MeasureJcl;
begin
  var s := TStringList.Create;
  var g: ISafeGuard;
  Guard(s, g);
  for var i := 1 to ADD_COUNT do
    s.Add(i.ToString);
end;
{$ENDIF}

procedure MeasureClassic;
begin
  var s := TStringList.Create;
  try
    for var i := 1 to ADD_COUNT do
      s.Add(i.ToString);
  finally
    s.Free;
  end;
end;

procedure MeasureClassicCopy;
begin
  var s := TStringList.Create;
  try
    for var i := 1 to ADD_COUNT do begin
      var s1 := s;
      s1.Add(i.ToString);
    end;
  finally
    s.Free;
  end;
end;

procedure MeasureMyUnique;
begin
  var s := Ptr.MakeUnique<TStringList>();
  for var i := 1 to ADD_COUNT do
    s.Instance.Add(i.ToString);
end;

procedure MeasureMyShared;
begin
  var s := Ptr.MakeShared<TStringList>();
  for var i := 1 to ADD_COUNT do
    s.Instance.Add(i.ToString);
end;

procedure MeasureMySharedCopy;
begin
  var s := Ptr.MakeShared<TStringList>();
  for var i := 1 to ADD_COUNT do begin
    var s1 := s;
    s1.Instance.Add(i.ToString);
  end;
end;

procedure Main;
var
  sw: TStopwatch;
  i: Integer;
begin
  {$IFDEF SPRING4D}
  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureSpring;
  Writeln('Spring4D ' + sw.ElapsedMilliseconds.ToString());
  {$ENDIF}

  {$IFDEF JCL}
  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureJcl;
  Writeln('Jcl ' + sw.ElapsedMilliseconds.ToString());
  {$ENDIF}

  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureClassic;
  Writeln('Classic ' + sw.ElapsedMilliseconds.ToString());

  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureMyUnique;
  Writeln('Unique ' + sw.ElapsedMilliseconds.ToString());

  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureMyShared;
  Writeln('Shared ' + sw.ElapsedMilliseconds.ToString());

  Writeln;
  Writeln('COPY');

  {$IFDEF SPRING4D}
  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureSpringCopy;
  Writeln('Spring4D ' + sw.ElapsedMilliseconds.ToString());
  {$ENDIF}

  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureClassicCopy;
  Writeln('Classic ' + sw.ElapsedMilliseconds.ToString());

  sw := TStopwatch.StartNew;
  for i := 1 to CREATE_COUNT do
    MeasureMySharedCopy;
  Writeln('Shared ' + sw.ElapsedMilliseconds.ToString());
end;

begin
  Main;

  Writeln;
  Write('Done.. press <Enter> key to quit.');
  Readln;
end.
