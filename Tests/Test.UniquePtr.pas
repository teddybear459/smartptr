unit Test.UniquePtr;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TUniquePtrTest = class
  public

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure IsUnique;

    [Test]
    procedure AsBoolean;

    [Test]
    procedure Release;

    [Test]
    procedure Positive;

    [Test]
    procedure AsResult;

    [Test]
    procedure Null;
  end;

implementation

uses
  System.Classes,
  SmartPtr;

procedure TUniquePtrTest.Setup;
begin

end;

procedure TUniquePtrTest.TearDown;
begin

end;

procedure TUniquePtrTest.AsResult;
begin
  var func := function: UniquePtr<TStringList>
    begin
      var s := Ptr.MakeUnique<TStringList>();
      (+s).Add('string');
      Exit(s);
    end;

  var list := func();
  Assert.AreEqual('string', (+list)[0]);
end;

procedure TUniquePtrTest.IsUnique;
begin
  var obj1 := Ptr.MakeUnique<TStringList>();

  Assert.IsNotNull(obj1.Instance);
  Assert.IsTrue(obj1);

  var obj2 := obj1;

  Assert.IsNull(obj1.Instance);
  Assert.IsFalse(obj1);

  Assert.IsNotNull(obj2.Instance);
  Assert.IsTrue(obj2);
end;

procedure TUniquePtrTest.Null;
begin
  var obj1 := Ptr.MakeUnique<TStringList>();
  obj1 := nil;

  Assert.IsFalse(obj1);
end;

procedure TUniquePtrTest.AsBoolean();
begin
  var obj1 := Ptr.MakeUnique<TStringList>();

  Assert.IsTrue(obj1);

  var obj2 := obj1;

  Assert.IsFalse(obj1);
  Assert.IsTrue(obj2);
end;

procedure TUniquePtrTest.Release();
begin
  var obj := Ptr.MakeUnique<TStringList>();
  Assert.IsTrue(obj);

  var List1 := obj.Value;
  var List2 := obj.Release();
  Assert.IsFalse(obj);
  Assert.AreSame(List1, List2);

  obj := List1;
  Assert.IsTrue(obj);
end;

procedure TUniquePtrTest.Positive;
begin
  var p := Ptr.MakeUnique<TStringList>();
  var obj := +p;

  Assert.AreSame(p.Value, obj);
end;


initialization
  TDUnitX.RegisterTestFixture(TUniquePtrTest);

end.
