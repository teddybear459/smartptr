unit Test.WeakPtr;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TWeakPtrTest = class
  public

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure FromShared;

    [Test]
    procedure EmptyWeak;

    [Test]
    procedure Reset;

    [Test]
    procedure Expired;
  end;

implementation

uses
  System.Classes,
  SmartPtr;

{ TWeakPtrTest }

procedure TWeakPtrTest.EmptyWeak;
begin
  var weakList: WeakPtr<TStringList>;
  var list := weakList.Lock();
  Assert.IsFalse(list);
end;

procedure TWeakPtrTest.Expired;
begin
  var list := Ptr.MakeShared<TStringList>();
  var weakList := WeakPtr<TStringList>(list);

  list.Reset();
  Assert.IsTrue(weakList.Expired, 'on Reset');

  var sharedList := weakList.Lock();
  Assert.IsTrue(weakList.Expired, 'on new SharedPtr<T>');
  Assert.IsFalse(sharedList);
end;

procedure TWeakPtrTest.FromShared;
begin
  var list := Ptr.MakeShared<TStringList>();
  var weakList := WeakPtr<TStringList>(list);

  var sharedList := weakList.Lock();
  Assert.IsTrue(sharedList);
end;

procedure TWeakPtrTest.Reset;
begin
  var list := Ptr.MakeShared<TStringList>();
  var weakList := WeakPtr<TStringList>(list);

  weakList.Reset();
  var sharedList := weakList.Lock();
  Assert.IsFalse(sharedList);
end;

procedure TWeakPtrTest.Setup;
begin

end;

procedure TWeakPtrTest.TearDown;
begin

end;

end.
