unit Test.SharedPtr;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TSharedPtrTest = class
  public

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure FromUnique;

    [Test]
    procedure AsBoolean;

    [Test]
    procedure Reset;

    [Test]
    procedure Positive;
  end;

implementation

uses
  System.Classes,
  SmartPtr;

procedure TSharedPtrTest.Setup;
begin

end;

procedure TSharedPtrTest.TearDown;
begin

end;

procedure TSharedPtrTest.FromUnique;
begin
  var list := TStringList.Create();
  var obj1 := Ptr.MakeUnique(list);
  var obj2: SharedPtr<TStringList> := obj1;
  Assert.IsFalse(obj1);
  Assert.IsTrue(obj2)
end;

procedure TSharedPtrTest.AsBoolean;
begin
  var obj1 := Ptr.MakeShared<TStringList>();
  var obj2 := obj1;
  Assert.IsNotNull(obj1.Instance);
  Assert.IsTrue(obj1);
end;

procedure TSharedPtrTest.Reset();
begin
  var obj1 := Ptr.MakeShared<TStringList>();
  var obj2 := obj1;

  Assert.IsTrue(obj1);
  Assert.IsTrue(obj2);

  obj1.Reset();

  Assert.IsFalse(obj1);
  Assert.IsTrue(obj2);

  obj2.Reset();

  Assert.IsFalse(obj1);
  Assert.IsFalse(obj2);
end;

procedure TSharedPtrTest.Positive;
begin
  var p := Ptr.MakeUnique<TStringList>();
  var obj := +p;
  obj.Add('string');
  (+p).Add('string');
  Assert.AreEqual('string,string', (+p).CommaText);
end;


initialization
  TDUnitX.RegisterTestFixture(TSharedPtrTest);

end.
