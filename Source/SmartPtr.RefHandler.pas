unit SmartPtr.RefHandler;

interface

{$I Smart4D.inc}

uses
  System.Math;

type
  TRefCount = packed record
    case Boolean of
      True: (Value: Int64);
      False: (SharedCount, WeakCount: Int32);
    end;

  PRefHandler = ^TRefHandler;

  TRefHandler = record
    const
      LockFlag = Int64($8000000000000000);

    var
      Instance: TObject;
      RefCount: TRefCount;
{$IFDEF GC}
      NextFreeHandler: PRefHandler;

    class var
      FirstFreeHandler: PRefHandler;

    class constructor Create();
    class destructor Destroy();

    class procedure New(out Ref: PRefHandler); static; inline;
    class procedure Dispose(var Ref: PRefHandler); static; inline;
    class procedure Collect(); static;
{$ENDIF}

    class procedure Initialize(out Dest: PRefHandler; Instance: TObject); static; inline;

    class procedure _AtomicAddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure _AtomicAddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure _AtomicRelease(var Dest: PRefHandler); static; inline;
    class procedure _AtomicReleaseWeak(var Dest: PRefHandler); static; inline;

    class procedure _AddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure _AddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure _Release(var Dest: PRefHandler); static; inline;
    class procedure _ReleaseWeak(var Dest: PRefHandler); static; inline;

    class procedure AddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure AddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler); static; inline;
    class procedure Release(var Dest: PRefHandler); static; inline;
    class procedure ReleaseWeak(var Dest: PRefHandler); static; inline;
  end;

implementation

{ TReferenceHandler }

{$IFDEF GC}

class constructor TRefHandler.Create();
begin
  TRefHandler.FirstFreeHandler := nil;
end;

class destructor TRefHandler.Destroy();
begin
  TRefHandler.Collect();
end;

class procedure TRefHandler.New(out Ref: PRefHandler);
var
  NextRef: Pointer;
begin
  repeat
    NextRef := nil;
    Ref := TRefHandler.FirstFreeHandler;
    if Ref <> nil then begin
      NextRef := Ref.NextFreeHandler;
    end;
  until AtomicCmpExchange(TRefHandler.FirstFreeHandler, NextRef, Ref) = Ref;

  if Ref = nil then begin
    System.New(Ref);
  end;
end;

class procedure TRefHandler.Dispose(var Ref: PRefHandler);
var
  FirstRef: Pointer;
begin
  repeat
    FirstRef := TRefHandler.FirstFreeHandler;
    Ref.NextFreeHandler := FirstRef;
  until AtomicCmpExchange(TRefHandler.FirstFreeHandler, Ref, FirstRef) = FirstRef;
end;

class procedure TRefHandler.Collect();
begin
  while TRefHandler.FirstFreeHandler <> nil do begin
    var Next := TRefHandler.FirstFreeHandler.NextFreeHandler;
    System.Dispose(TRefHandler.FirstFreeHandler);
    TRefHandler.FirstFreeHandler := Next;
  end;
end;

{$ENDIF}

class procedure TRefHandler.Initialize(out Dest: PRefHandler; Instance: TObject);
begin
  Dest := nil;
  if Instance <> nil then begin
    New(Dest);
    Dest.Instance := Instance;
    Dest.RefCount.Value := 1;
{$IFDEF GC}
    Dest.NextFreeHandler := nil;
{$ENDIF}
  end;
end;

class procedure TRefHandler._AtomicAddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
var
  Ref: TRefCount;
  Expected: Int64;
begin
  Dest := nil;
  if Src <> nil then begin
    with Src^, Ref do begin
      repeat
        Expected := RefCount.Value and (not LockFlag);
        Value := Expected;
        Dest := nil;
        if SharedCount > 0 then begin
          Dest := Src;
          Inc(SharedCount);
        end;
      until AtomicCmpExchange(RefCount.Value, Value, Expected) = Expected;
    end;
  end;
end;

class procedure TRefHandler._AtomicAddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
var
  Ref: TRefCount;
  Expected: Int64;
begin
  Dest := nil;
  if Src <> nil then begin
    with Src^, Ref do begin
      repeat
        Expected := RefCount.Value and (not LockFlag);
        Value := Expected;
        Dest := nil;
        if SharedCount > 0 then
          Dest := Src;
        Inc(WeakCount)
      until AtomicCmpExchange(RefCount.Value, Value, Expected) = Expected;
    end;
  end;
end;

class procedure TRefHandler._AtomicRelease(var Dest: PRefHandler);
var
  Ref: TRefCount;
  Expected: Int64;
  LastRef: Boolean;
  NeedLock: Boolean;
  InstanceToFree: TObject;
begin
  if Dest <> nil then begin
    with Dest^, Ref do begin
      repeat
        Value := RefCount.Value and (not LockFlag);
        Expected := Value;
        Dec(SharedCount);
        LastRef := (SharedCount = 0) and (Instance <> nil);
        NeedLock := LastRef and (WeakCount > 0);
      until AtomicCmpExchange(RefCount.Value, IfThen(NeedLock, Value or LockFlag, Value), Expected) = Expected;

      InstanceToFree := nil;
      if LastRef then begin
        InstanceToFree := Instance;
        Instance := nil;
        if NeedLock then
          AtomicExchange(RefCount.Value, Value);
      end;

      if Value = 0 then begin
        Dispose(Dest);
      end;

      Dest := nil;
      InstanceToFree.Free();
    end;
  end;
end;

class procedure TRefHandler._AtomicReleaseWeak(var Dest: PRefHandler);
begin
  if Dest <> nil then begin
    with Dest^ do begin
      var Ref: TRefCount;
      with Ref do begin
        var Expected: Int64;
        repeat
          Value := RefCount.Value and (not LockFlag);
          Expected := Value;
          Dec(WeakCount);
        until AtomicCmpExchange(RefCount.Value, Value, Expected) = Expected;

        if Value = 0 then begin
          Dispose(Dest);
        end;

        Dest := nil;
      end;
    end;
  end;
end;

class procedure TRefHandler._AddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
begin
  Dest := nil;
  if Src <> nil then begin
    with Src^.RefCount do begin
      if SharedCount > 0 then begin
        AtomicIncrement(SharedCount);
        Dest := Src;
      end;
    end;
  end;
end;

class procedure TRefHandler._AddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
begin
  Dest := nil;
  if Src <> nil then begin
    with Src.RefCount do begin
      if SharedCount > 0 then begin
        AtomicIncrement(WeakCount);
        Dest := Src;
      end;
    end;
  end;
end;

class procedure TRefHandler._Release(var Dest: PRefHandler);
begin
  if Dest <> nil then begin
    with Dest^, RefCount do begin
      var InstanceToFree: TObject := nil;
      AtomicDecrement(SharedCount);
      if SharedCount = 0 then begin
        InstanceToFree := Instance;
        Instance := nil;
      end;

      if Value = 0 then begin
        Dispose(Dest);
      end;

      Dest := nil;
      InstanceToFree.Free();
    end;
  end;
end;

class procedure TRefHandler._ReleaseWeak(var Dest: PRefHandler);
begin
  if Dest <> nil then begin
    with Dest.RefCount do begin
      AtomicDecrement(WeakCount);

      if Value = 0 then begin
        Dispose(Dest);
      end;

      Dest := nil;
    end;
  end;
end;

class procedure TRefHandler.AddRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
begin
  {$IFDEF ATOMIC}
  _AtomicAddRef(Dest, Src);
  {$ELSE}
  _AddRef(Dest, Src);
  {$ENDIF}
end;

class procedure TRefHandler.AddWeakRef(out Dest: PRefHandler; const [ref] Src: PRefHandler);
begin
  {$IFDEF ATOMIC}
  _AtomicAddWeakRef(Dest, Src);
  {$ELSE}
  _AddWeakRef(Dest, Src);
  {$ENDIF}
end;

class procedure TRefHandler.Release(var [ref] Dest: PRefHandler);
begin
  {$IFDEF ATOMIC}
  _AtomicRelease(Dest);
  {$ELSE}
  _Release(dest);
  {$ENDIF}
end;

class procedure TRefHandler.ReleaseWeak(var [ref] Dest: PRefHandler);
begin
  {$IFDEF ATOMIC}
  _AtomicReleaseWeak(Dest);
  {$ELSE}
  _ReleaseWeak(Dest);
  {$ENDIF}
end;


end.
