unit SmartPtr;

interface

uses
  System.SysUtils,
  System.Math,
  SmartPtr.RefHandler;

type
  UniquePtr<T:class> = record
  strict private
    FInstance: T;
  public
    class operator Initialize(out Dest: UniquePtr<T>);
    class operator Finalize(var Dest: UniquePtr<T>);
    class operator Assign(var Dest: UniquePtr<T>; const [ref] Src: UniquePtr<T>);
    class operator Implicit(const [ref] Src: UniquePtr<T>): Boolean; inline;
    class operator Implicit(const [ref] Src: UniquePtr<T>): T; inline;
    class operator Implicit(const Src: T): UniquePtr<T>; inline;
    class operator Positive(const [ref] Src: UniquePtr<T>): T; inline;
    constructor Create(const Instance: T);
    function Release(): T; inline;
    procedure Reset(); inline;
    property Instance: T read FInstance;
    property Value: T read FInstance;
  end;

  SharedPtr<T:class> = record
  private
    FRefHandler: PRefHandler;
  strict private
    function GetInstance(): T;
  public
    class operator Initialize(out Dest: SharedPtr<T>);
    class operator Finalize(var Dest: SharedPtr<T>);
    class operator Assign(var Dest: SharedPtr<T>; const [ref] Src: SharedPtr<T>);
    class operator Implicit(const [ref] Src: UniquePtr<T>): SharedPtr<T>; inline;
    class operator Implicit(const [ref] Src: SharedPtr<T>): Boolean; inline;
    class operator Implicit(const [ref] Src: SharedPtr<T>): T; inline;
    class operator Positive(const [ref] Src: SharedPtr<T>): T; inline;
    constructor Create(const Instance: T);
    procedure Reset();
    property Instance: T read GetInstance;
    property Value: T read GetInstance;
  end;

  WeakPtr<T:class> = record
  strict private
    FRefHandler: PRefHandler;
    function GetExpired(): Boolean;
    function GetUseCount(): Integer;
  public
    class operator Initialize(out Dest: WeakPtr<T>);
    class operator Finalize(var Dest: WeakPtr<T>);
    class operator Assign(var Dest: WeakPtr<T>; const [ref] Src: WeakPtr<T>);
    class operator Implicit(const [ref] Src: SharedPtr<T>): WeakPtr<T>;
    class operator Implicit(const [ref] Src: WeakPtr<T>): SharedPtr<T>;
    procedure Reset();
    function Lock(): SharedPtr<T>; inline;
    property Expired: Boolean read GetExpired;
    property UseCount: Integer read GetUseCount;
  end;

  Ptr = record
    class function MakeUnique<T:class>(const Instance: T): UniquePtr<T>; overload; static;
    class function MakeUnique<T:class,constructor>(): UniquePtr<T>; overload; static;
    class function MakeShared<T:class>(const Instance: T): SharedPtr<T>; overload; static;
    class function MakeShared<T:class,constructor>(): SharedPtr<T>; overload; static;
  end;

implementation

{ UniquePtr<T> }

constructor UniquePtr<T>.Create(const Instance: T);
begin
  FInstance := Instance;
end;

class operator UniquePtr<T>.Initialize(out Dest: UniquePtr<T>);
begin
  with Dest do begin
    FInstance := nil;
  end;
end;

class operator UniquePtr<T>.Finalize(var Dest: UniquePtr<T>);
begin
  with Dest do begin
    FreeAndNil(FInstance);
  end;
end;

class operator UniquePtr<T>.Assign (var Dest: UniquePtr<T>; const [ref] Src: UniquePtr<T>);
begin
  with Dest do begin
    Reset();
    FInstance := Src.Release();
  end;
end;

class operator UniquePtr<T>.Implicit(const [ref] Src: UniquePtr<T>): Boolean;
begin
  with Src do begin
    Exit(FInstance <> nil);
  end;
end;

class operator UniquePtr<T>.Implicit(const [ref] Src: UniquePtr<T>): T;
begin
  with Src do begin
    Exit(FInstance);
  end;
end;

class operator UniquePtr<T>.Implicit(const Src: T): UniquePtr<T>;
begin
  with Result do begin
    Reset();
    FInstance := Src;
  end;
end;

class operator UniquePtr<T>.Positive(const [ref] Src: UniquePtr<T>): T;
begin
  with Src do begin
    Exit(FInstance);
  end;
end;

function UniquePtr<T>.Release(): T;
begin
  Result := FInstance;
  FInstance := nil;
end;

procedure UniquePtr<T>.Reset();
begin
  FreeAndNil(FInstance);
end;

{ SharedPtr<T> }

constructor SharedPtr<T>.Create(const Instance: T);
begin
  TRefHandler.Initialize(FRefHandler, Instance);
end;

class operator SharedPtr<T>.Initialize(out Dest: SharedPtr<T>);
begin
  with Dest do begin
    FRefHandler := nil;
  end;
end;

class operator SharedPtr<T>.Finalize(var Dest: SharedPtr<T>);
begin
  with Dest do begin
    TRefHandler.Release(FRefHandler);
  end;
end;

class operator SharedPtr<T>.Assign(var Dest: SharedPtr<T>; const [ref] Src: SharedPtr<T>);
begin
  with Dest do begin
    if FRefHandler <> Src.FRefHandler then begin
      TRefHandler.Release(FRefHandler);
      TRefHandler.AddRef(FRefHandler, Src.FRefHandler);
    end;
  end;
end;

class operator SharedPtr<T>.Implicit(const [ref] Src: UniquePtr<T>): SharedPtr<T>;
begin
  var Instance := Src.Release();
  TRefHandler.Initialize(Result.FRefHandler, Instance);
end;

class operator SharedPtr<T>.Implicit(const [ref] Src: SharedPtr<T>): Boolean;
begin
  with Src do begin
    Result := (FRefHandler <> nil) and (FRefHandler.Instance <> nil);
  end;
end;

class operator SharedPtr<T>.Implicit(const [ref] Src: SharedPtr<T>): T;
begin
  with Src do begin
    if FRefHandler = nil then
      Exit(nil);
    Result := T(FRefHandler.Instance);
  end;
end;

class operator SharedPtr<T>.Positive(const [ref] Src: SharedPtr<T>): T;
begin
  with Src do begin
    if FRefHandler = nil then
      Exit(nil);
    Result := T(FRefHandler.Instance);
  end;
end;

procedure SharedPtr<T>.Reset();
begin
  TRefHandler.Release(FRefHandler);
end;

function SharedPtr<T>.GetInstance(): T;
begin
  if FRefHandler = nil then
    Exit(nil);
  Result := T(FRefHandler.Instance);
end;

{ WeakPtr<T> }

class operator WeakPtr<T>.Initialize(out Dest: WeakPtr<T>);
begin
  with Dest do begin
    FRefHandler := nil;
  end;
end;

class operator WeakPtr<T>.Finalize(var Dest: WeakPtr<T>);
begin
  with Dest do begin
    TRefHandler.ReleaseWeak(FRefHandler);
  end;
end;

class operator WeakPtr<T>.Assign(var Dest: WeakPtr<T>; const [ref] Src: WeakPtr<T>);
begin
  with Dest do begin
    if FRefHandler <> Src.FRefHandler then begin
      Reset();
      TRefHandler.AddWeakRef(FRefHandler, Src.FRefHandler);
    end;
  end;
end;

class operator WeakPtr<T>.Implicit(const [ref] Src: SharedPtr<T>): WeakPtr<T>;
begin
  with Result do begin
    TRefHandler.AddWeakRef(FRefHandler, Src.FRefHandler);
  end;
end;

class operator WeakPtr<T>.Implicit(const [ref] Src: WeakPtr<T>): SharedPtr<T>;
begin
  with Result do begin
    TRefHandler.AddRef(FRefHandler, Src.FRefHandler);
  end;
end;

function WeakPtr<T>.GetExpired(): Boolean;
begin
  if FRefHandler = nil then
    Exit(True);
  Exit(FRefHandler.RefCount.SharedCount = 0);
end;

function WeakPtr<T>.GetUseCount(): Integer;
begin
  if FRefHandler = nil then
    Exit(0);
  Exit(FRefHandler.RefCount.SharedCount);
end;

procedure WeakPtr<T>.Reset();
begin
  TRefHandler.ReleaseWeak(FRefHandler);
end;

function WeakPtr<T>.Lock(): SharedPtr<T>;
begin
  Result := Self;
end;

{ Ptr }

class function Ptr.MakeUnique<T>(const Instance: T): UniquePtr<T>;
begin
  Result := UniquePtr<T>.Create(Instance);
end;

class function Ptr.MakeUnique<T>(): UniquePtr<T>;
begin
  Result := UniquePtr<T>.Create(T.Create());
end;

class function Ptr.MakeShared<T>(const Instance: T): SharedPtr<T>;
begin
  Result := SharedPtr<T>.Create(Instance);
end;

class function Ptr.MakeShared<T>(): SharedPtr<T>;
begin
  Result := SharedPtr<T>.Create(T.Create());
end;

end.
