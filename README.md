# Smart pointers for Delphi

Implements smart pointers for Delphi 10.4 Sydney with new managed records.

---
## Installation

You have two methods:

- Build and install the SmartPointers.bpl package. Update the library path in Delphi options.
- Copy SmartPtr.pas and SmartPtr.RefHandler in your project.

## Current Version

**1.0** - *August 3, 2020*

---
## A quick sample

No need of `try ... finally ... end;` anymore.  

```delphi
uses
  SmartPtr;
  
procedure AddStr(List: TStringList);
begin
  List.Add('four');
end;
  
procedure Main();
begin
  var MyList := Ptr.MakeUnique<TStringList>();
  
  MyList.Instance.Add('one');
  MuList.Value.Add('two');
  (+MyList).Add('three');
  
  AddStr(MyList);
  
  Writeln((+MyList).CommaText); // one,two,three,four
  // MyList is released at the end of the block.
end;
```

---
## Many way to create a smart pointer

The first two are the better.

```delphi
  // Need TMyType to have a parameterless constructor
  var MyObj := Ptr.MakeUnique<TMyType>(); 
  
  // More parameters
  var MyObj := Ptr.MakeUnique(TMyType.Create(Param1, Param2)); 
  
  // Ugly !
  var MyObj: UniquePtr<TMyType>;
  MyObj := TMyType.Create();
  
  var MyObj := UniquePtr<TMyType>.Create(TMyType.Create());
  
  // The complete version
  var MyObj: UniquePtr<TMyType> := Ptr.MakeUnique<TMyType>(TMyType.Create());
```

---
## UniquePtr<T> are unique

UniquePtr do not share the owernership. It is moved from the first to the second.

```delphi
  var p1 := Ptr.MakeUnique<TStringList>();
  
  Assert(p1.Value <> nil); // OK
  Assert(p1); // OK
  
  var p2 := p1;
  
  Assert(p2.Value <> nil); // OK
  Assert(p1.Value <> nil); // EAssertionFailed !!!!
  Assert(not p1); // OK
```

Functions can return UniquePtr<T>.

```delphi
  function GetList(): TStringList;
  begin
    // MyList will be freed if an exception occurs
    var MyList := Ptr.MakeUnique<TStringList>();
    ...
    // Release the ownership and return the instance
    Exit(MyList.Release());
  end;

  function GetUniqueList(): UniquePtr<TStringList>;
  begin
    Exit(Ptr.MakeUnique<TStringList>());
  end;

  // OK, list: UniquePtr<TStringList>
  var list := GetUniqueList();

  // OK, list takes the ownership.
  var list: TUniquePtr<TStringList> := GetList();

  // KO, will compile and run but otherList is invalid
  // The instance has been freed when the temporary UniquePtr as gone.
  var list: TStringList := GetUniqueList();
```

---
## SharedPtr<T>

SharedPtr<T> share the ownership with other SharedPtr<T> but not with UniquePtr<T>;

```delphi
  var u := Ptr.MakeUnique<TStringList>();

  Asser(u) // OK
  
  var p1: SharedPtr<TStringList> := u;
  
  Assert(not u); // OK, u = nil
  Assert(p1); // OK
  
  var p2 := p1;
  
  Assert(p1); // OK
  Assert(p2); // OK
```

Functions can return SharedPtr<T>.

```delphi
  function GetList(): TStringList;
  begin
    // MyList will be freed if an exception occurs
    var MyList := Ptr.MakeUnique<TStringList>();
    ...
    // Release the ownership and return the instance
    Exit(MyList.Release());
  end;

  function GetSharedList(): SharedPtr<TStringList>;
  begin
    Exit(Ptr.MakeShared<TStringList>());
  end;

  // OK
  var list: TSharedPtr<TStringList> := GetSharedList();

  // OK, list takes the ownership.
  var list: TSharedPtr<TStringList> := GetList();

  // KO, will compile and run but otherList is invalid
  // The instance has been freed when the temporary SharedPtr as gone.
  var list: TStringList := GetSharedList();
```

---
## WeakPtr<T>

WeakPtr<T> don't take or share the ownership.
The object is freed when the last SharedPtr<T> is gone.

```delphi
  var list: Ptr.MakeShared<TStringList>();
  var weakList := WeakPtr<TStringList>(list);

  list.Reset();
  Assert(weakList.Expired);

  var sharedList := weakList.Lock();
  Assert.IsFalse(sharedList);
  
```

---
## More to come

- Thread Safety
